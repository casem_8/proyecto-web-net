using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PrimerWebApp.Pages
{
    public class ContactModel : PageModel
    {
        public bool hasData = false; // Hay datos en el formulario o no
        public string firstName = "";
        public string lastName = "";
        public string message = "";

        // M�todo que se ejecuta cuando se solicita la p�gina Contact
        public void OnGet()
        {
        }

        // M�todo que se ejecuta cuando se env�a el formulario
        public void OnPost() 
        {
            hasData = true;
            firstName = Request.Form["firstname"];
            lastName = Request.Form["lastname"];
            message = Request.Form["message"];
        }
    }
}
